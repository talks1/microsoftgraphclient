// Make sure you replace these values from the copied values of you app

// const APP_ID = '9c3a028e-f39a-428f-93e0-3a1255e1e2d0';
// const APP_SECRET = 'hRIcNA6TXJZF8AB/Zleaaqhs@8m5Mv/[';
// const TENANT_ID='951b1ed2-d31c-4c2a-9dd6-8ea6137ceb9d'
// const TOKEN_ENDPOINT =`https://login.microsoftonline.com/${TENANT_ID}/oauth2/v2.0/token`;
// const GROUP = '33b368af-ec25-4f3d-bf7c-8088e777fcef'

const APP_ID = '7976efb8-4c39-44da-a132-efffbd26e3e3';
const APP_SECRET = 'M55k9xLJ2kjix_P3-4lHQ26NWmRFZR-Po_';
const TENANT_ID='adb42340-ff49-4969-a827-a0ddecd6174c'
const TOKEN_ENDPOINT =`https://login.microsoftonline.com/${TENANT_ID}/oauth2/v2.0/token`;
const GROUP = '99520478-4d69-4e0e-9be1-2b71e9511876'

const MS_GRAPH_SCOPE = 'https://graph.microsoft.com/.default';


const axios = require('axios');
const qs = require('qs');

const postData = {
  client_id: APP_ID,
  scope: MS_GRAPH_SCOPE,
  client_secret: APP_SECRET,
  grant_type: 'client_credentials'
};

axios.defaults.headers.post['Content-Type'] =
  'application/x-www-form-urlencoded';

let token = '';

axios
  .post(TOKEN_ENDPOINT, qs.stringify(postData))
  .then(response => {
    console.log(response.data);
    getGroups(response.data)
  })
  .catch(error => {
    console.log(error);
  });

async function getGroups(token){
    let endpoint = `https://graph.microsoft.com/v1.0/groups/${GROUP}/members`
    let response = await axios.get(endpoint, {
            headers:{'Authorization': `Bearer ${token.access_token}` ,
            Host: 'graph.microsoft.com'
        }
    })
    response.data.value.forEach(v=>{
        console.log(v)
    })
}